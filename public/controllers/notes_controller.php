<?php
  class NotesController {
    public function index() {
      // we store notes
      $notes = Note::all();
      require_once('views/notes/index.php');
    }

    public function show() {
      // we expect a url of form ?controller=posts&action=show&id=x
      // call error if no id
      if (!isset($_GET['id']))
        return call('pages', 'error');

      // find the note by id
      $note = Note::find($_GET['id']);
      require_once('views/notes/show.php');
    }
  }
?>
