<?php
  // controller for static pages
  class PagesController {
    public function home() {
      // give us some identity
      $ident = 'root';
      require_once('views/pages/home.php');
    }

    public function error() {
      require_once('views/pages/error.php');
    }
  }
?>
