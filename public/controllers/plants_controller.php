<?php
  class PlantsController {
    public function index() {
      // we store plants
      $plants = Plant::all();
      require_once('views/plants/index.php');
    }

    public function show() {
      // we expect a url of form ?controller=posts&action=show&id=x
      if (!isset($_GET['id']))
        return call('pages', 'error');

      $plant = Plant::find($_GET['id']);
      require_once('views/plants/show.php');
    }
  }
?>
