<?php
  class PlotsController {
    public function index() {
      $plots = Plot::all();
      require_once('views/plots/index.php');
    }

    public function show() {
      if (!isset($_GET['id']))
        return call('pages', 'error');

      $plot = Plot::find($_GET['id']);
      require_once('views/plots/show.php');
    }
  }
?>
