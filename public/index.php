<?php
  // require our connection
  require_once('../src/connection.php');

  // check for a controller and action
  if (isset($_GET['controller']) && isset($_GET['action'])) {
    $controller = $_GET['controller'];
    $action = $_GET['action'];
  } else {
    $controller = 'pages';
    $action ='hone';
  }

  // load out layout
  require_once('views/layout.php');
?>
