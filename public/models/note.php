<?php
  class Note {
    // attributes
    public $id;
    public $title;
    public $body;
    public $author_id;

    public function __construct($id, $title, $body, $author_id) {
      $this->id               = $id;
      $this->title            = $title;
      $this->body             = $body;
      $this->author_id        = $autho_id;
    }

    public static function all() {
      $list = [];
      $db = Db::getInstance();
      $req = $db->query('SELECT id, title, body, author_id FROM note');

      foreach($req->fetch_all($resulttype = MYSQLI_ASSOC) as $note) {
        $list[] = new Note($note['id'], $note['title'], $note['body'], $note['author_id']);
      }

      return $list;
    }

    public static function find($id) {
      $db = Db::getInstance();

      // check for integer
      $id = intval($id);
      $req = $db->prepare("SELECT note.id, title, body, author_id FROM note LEFT JOIN user ON note.author_id = user.id WHERE note.author_id = ?");
      $req->bind_param('i', $id);

      $req->execute();

      //get an array
      $res = $req->get_result();
      $note = $res->fetch_assoc();

      return new Note($note['id'], $note['title'], $note['body'], $note['author_id']);
    }
  }
?>
