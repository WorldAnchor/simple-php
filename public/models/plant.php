<?php
  class Plant {
    // attributes
    public $id;
    public $common_name;
    public $scientific_name;

    public function __construct($id, $common_name, $scientific_name) {
      $this->id              = $id;
      $this->common_name     = $common_name;
      $this->scientific_name = $scientific_name;
    }

    public static function all() {
      $list = [];
      $db = Db::getInstance();
      $req = $db->query('SELECT id, common_name, scientific_name FROM plant');

      foreach($req->fetch_all($resulttype = MYSQLI_ASSOC) as $plant) {
        $list[] = new Plant($plant['id'], $plant['common_name'], $plant['scientific_name']);
      }

      return $list;
    }

    public static function find($id) {
      $db = Db::getInstance();

      // check for integer
      $id = intval($id);
      $req = $db->prepare("SELECT id, common_name, scientific_name FROM plant WHERE id = ?");
      $req->bind_param('i', $id);

      $req->execute();

      //get an array
      $res = $req->get_result();
      $plant = $res->fetch_assoc();

      return new Plant($plant['id'], $plant['common_name'], $plant['scientific_name']);
    }
  }
?>
