<?php
  class Plot {
    // attributes
    public $id;
    public $long;
    public $lat;
    public $owner_id;
    public $owner;
    public $created;

    public function __construct($id, $long, $lat, $owner_id, $created, $owner) {
      $this->id              = $id;
      $this->long            = $long;
      $this->lat             = $long;
      $this->owner_id        = $owner_id;
      $this->created         = $created;
      $this->owner           = $owner;
    }

    public static function all() {
      $list = [];
      $db = Db::getInstance();
      $req = $db->query('SELECT id, X(location) AS `long`, Y(location) AS `lat`, owner_id, created FROM plot');

      foreach($req->fetch_all($resulttype = MYSQLI_ASSOC) as $plot) {
        $list[] = new Plot($plot['id'], $plot['long'], $plot['lat'], $plot['owner_id'], $plot['created']);
      }

      return $list;
    }

    public static function find($id) {
      $db = Db::getInstance();

      // check for integer
      $id = intval($id);
      $req = $db->prepare("SELECT plot.id, X(location) AS `long`, Y(location) AS `lat`, owner_id, name, plot.created FROM plot LEFT JOIN user ON plot.owner_id = user.id WHERE plot.id = ?");
      $req->bind_param('i', $id);

      $req->execute();

      //get an array
      $res = $req->get_result();
      $plot = $res->fetch_assoc();

      return new Plot($plot['id'], $plot['long'], $plot['lat'], $plot['owner_id'], $plot['created'], $plot['name']);
    }
  }
?>
