<?php
  class User {
    // attributes
    public $id;
    public $name;

    public function __construct($id, $name) {
      $this->id   = $id;
      $this->name = $name;
    }

    public static function all() {
      $list = [];
      $db = Db::getInstance();
      $req = $db->query('SELECT id, name FROM user');

      // get our list of users
      foreach($req->fetch_all($resulttype = MYSQLI_ASSOC) as $user) {
        $list[] = new User($user['id'], $user['name']);
      }

      return $list;
    }

    public static function find($id) {
      $db = Db::getInstance();

      // check for integer
      $id = intval($id);
      $req = $db->prepare('SELECT id, name FROM user WHERE id = ?');
      $req->bind_param('i', $id);

      $req->execute();

      //get an array
      $res = $req->get_result();
      $user = $res->fetch_assoc();

      return new User($user['id'], $user['name']);
    }
  }
?>
