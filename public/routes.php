<?php
  function call($controller, $action)
  {
    // find the proper controller
    require_once('controllers/' . $controller . '_controller.php');

    // select our controller
    switch($controller)
    {
      case 'pages':
        $controller = new PagesController();
      break;
      case 'users':
        require_once('models/user.php');
        $controller = new UsersController();
      break;
      case 'plants':
        require_once('models/plant.php');
        $controller = new PlantsController();
      break;
      case 'plots':
        require_once('models/plot.php');
        $controller = new PlotsController();
      break;
      case 'notes':
        require_once('models/note.php');
        $controller = new NotesController();
      break;
    }

    // call the action required
    try {
      $controller-> { $action }();
    }
    catch (Exception $e) {
      echo $e->getMessage(), "/n";
    }
  }


  // white list controllers and pages
  $controllers = array('pages' => ['home','error'],
                       'users' => ['index', 'show', 'create', 'destroy'],
                       'plants' => ['index', 'show', 'create', 'destroy'],
                       'plots' => ['index', 'show', 'create', 'destroy'],
                       'notes' => ['index', 'show', 'create', 'destroy']);

  // check keys controller whitelist and execute action, or throw error
  if (array_key_exists($controller, $controllers)) {
    if (in_array($action, $controllers[$controller])) {
      call($controller, $action);
    } else {
      call('pages', 'error');
    }
  } else {
    call('pages', 'error');
  }
?>
