<DOCTYPE html>
<html>
  <head>
    <title>Plants & Plot Manager</title>
  </head>
  <body>
    <header>
      <a href='?controller=pages&action=home'>Home</a>
      <a href='?controller=users&action=index'>Users</a>
      <a href='?controller=plants&action=index'>Plants</a>
      <a href='?controller=plots&action=index'>Plots</a>
      <a href='?controller=notes&action=index'>Notes</a>
    </header>

    <?php require_once('routes.php'); ?>

    <footer>
    </footer>
  <body>
<html>
