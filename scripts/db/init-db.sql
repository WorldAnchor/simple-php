-- init-db.sql
-- Initialize application db.

-- Create the application user
CREATE USER IF NOT EXISTS 'shogerr-db'@'localhost' IDENTIFIED BY 'OMuPO7vViveSZMTh';
CREATE DATABASE IF NOT EXISTS `shogerr-db`;

-- Create the application database
GRANT ALL ON `shogerr-db`.* TO 'shogerr-db'@'localhost';
