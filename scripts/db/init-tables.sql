-- init-tables.sql
-- Create the tables of the application.

USE shogerr-db;

-- create plant table
CREATE TABLE IF NOT EXISTS `plant` (
  id int PRIMARY KEY AUTO_INCREMENT,
  common_name varchar(255) NOT NULL,
  scientific_name varchar(255),
  created datetime DEFAULT CURRENT_TIMESTAMP
);

-- create user table
CREATE TABLE IF NOT EXISTS `user` (
  id int PRIMARY KEY AUTO_INCREMENT,
  name varchar(255) NOT NULL,
  password varchar(255) NOT NULL,
  logged_in boolean DEFAULT 0,
  last_login timestamp,
  created datetime DEFAULT CURRENT_TIMESTAMP,
  UNIQUE KEY name (name)
);

-- create plot table
CREATE TABLE IF NOT EXISTS `plot` (
  id int PRIMARY KEY AUTO_INCREMENT,
  owner_id int NOT NULL DEFAULT 0,
  location point,
  created datetime DEFAULT CURRENT_TIMESTAMP,
  FOREIGN KEY (owner_id) REFERENCES user(id)
);

-- create note table
CREATE TABLE IF NOT EXISTS `note` (
  id int PRIMARY KEY AUTO_INCREMENT,
  title varchar(255) NOT NULL,
  body longtext,
  author_id int DEFAULT 0,
  created datetime DEFAULT CURRENT_TIMESTAMP,
  FOREIGN KEY (author_id) REFERENCES user(id)
);

-- create plant_detail table
CREATE TABLE IF NOT EXISTS `plant_detail` (
  id int PRIMARY KEY AUTO_INCREMENT,
  plant_id int NOT NULL,
  note_id int NOT NULL,
  FOREIGN KEY (plant_id) REFERENCES plant(id),
  FOREIGN KEY (note_id) REFERENCES note(id)
);

-- create plot_detail table
CREATE TABLE IF NOT EXISTS `plot_detail` (
  id int PRIMARY KEY AUTO_INCREMENT,
  plot_id int NOT NULL,
  note_id int NOT NULL,
  FOREIGN KEY (plot_id) REFERENCES plant(id),
  FOREIGN KEY (note_id) REFERENCES note(id)
);
