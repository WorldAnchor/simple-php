-- seed-db.sql
-- Seed the database with records for testing

USE shogerr-db;

-- create users
-- create a superuser, provide password hash for 'plants'
INSERT INTO `user` (name, password)
  VALUES ("superuser", "$5$k8rbZ5h/QvHDgm.$MBqL7sjOUC9ucHfWk6gzZM3DFuzAohW3R2GvnL4KDkC");

-- create a user, provide password hash for 'plants'
INSERT INTO `user` (name, password)
  VALUES ("Jill", "$5$k8rbZ5h/QvHDgm.$MBqL7sjOUC9ucHfWk6gzZM3DFuzAohW3R2GvnL4KDkC");

-- create a user, provide password hash for 'plants'
INSERT INTO `user` (name, password)
  VALUES ("Jane", "$5$k8rbZ5h/QvHDgm.$MBqL7sjOUC9ucHfWk6gzZM3DFuzAohW3R2GvnL4KDkC");

-- create some plants
INSERT INTO `plant` (common_name, scientific_name) VALUES ("Feather Reed Grass", "Calamagrostis");
INSERT INTO `plant` (common_name, scientific_name) VALUES ("Fern-leaf Yarrow", "Achillia fillipenulina");

