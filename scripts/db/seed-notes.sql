-- create some notes
use shogerr-db;

INSERT INTO `note` (author_id, title, body) VALUES ((SELECT id FROM user WHERE name="Jill"), "A general note", "This is a general note to all.");

INSERT INTO `note` (author_id, title, body) VALUES ((SELECT id FROM user WHERE name="Jane"), "Another note", "This is another note to all.");
