-- create some plots
use shogerr-db;

-- create plots
INSERT INTO `plot` (location, owner_id) VALUES (GeomFromText(CONCAT('POINT(', RAND() * 360 - 180, ' ', RAND() * 180 - 90, ')')), (SELECT id FROM user WHERE name="Jill"));
INSERT INTO `plot` (location, owner_id) VALUES (GeomFromText(CONCAT('POINT(', RAND() * 360 - 180, ' ', RAND() * 180 - 90, ')')), (SELECT id FROM user WHERE name="Jane"));
