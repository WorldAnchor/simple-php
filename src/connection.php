<?php
  class Db {
    private static $instance = NULL;

    private function __construct() {}
    private function __clone() {}

    public static function getInstance() {
      if (!isset(self::$instance))
      {
        $config = parse_ini_file('../config.ini');
        self::$instance = new mysqli($config['host'], $config['username'], $config['password'], $config['dbname']);
      }
      return self::$instance;
    }
  }
